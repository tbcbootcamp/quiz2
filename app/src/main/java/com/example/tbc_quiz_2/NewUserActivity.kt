package com.example.tbc_quiz_2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.add_new_user.*

class NewUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_new_user)
        init()
    }

    private fun init() {
        addUserButton.setOnClickListener {
            if (nameEditText.text.isNotEmpty() && surnameEditText.text.isNotEmpty() && emailEditText.text.isNotEmpty()) {
                addUserInfo()
            } else {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun addUserInfo() {
        intent.putExtra("name", nameEditText.text.toString())
        intent.putExtra("surname", surnameEditText.text.toString())
        intent.putExtra("email", emailEditText.text.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}