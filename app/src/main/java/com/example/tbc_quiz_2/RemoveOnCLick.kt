package com.example.tbc_quiz_2

interface RemoveOnClick {
    fun onClick(position: Int)
}