package com.example.tbc_quiz_2

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.OneShotPreDrawListener.add
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class UsersActivity : AppCompatActivity() {

    private val UsersList = mutableListOf<User>()
    private lateinit var adapter: RecyclerViewAdapter
    private val requestCode = 69

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(UsersList, object : RemoveOnClick {
            override fun onClick(position: Int) {
                removeUser(position)
            }
        })
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.adapter = adapter

        setData()

        addButton.setOnClickListener {
            val intent = Intent(this, NewUserActivity::class.java)
            startActivityForResult(intent, requestCode)
        }
    }

    private fun setData() {
        UsersList.add(
            0,
            User(
                "Sandro",
                "Kakhetelidze",
                "thesandro1998@gmail.com"
            )
        )
        UsersList.add(
            0,
            User(
                "Giga",
                "Sulkhanishvili",
                "sulkhanishviligiga3@gmail.com"
            )
        )
    }

    private fun removeUser(position: Int) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Remove User")
        alert.setMessage("Are you sure you want to remove user?")
        alert.setPositiveButton(
            "Yes"
        ) { _: DialogInterface, _: Int ->
            UsersList.removeAt(position)
            adapter.notifyItemRemoved(position)
        }
        alert.setNegativeButton("No") { _: DialogInterface, _: Int ->
        }
        alert.show()
    }
}
