package com.example.tbc_quiz_2

import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.add_new_user.view.*
import kotlinx.android.synthetic.main.item_recycler_view_layout.view.*

class RecyclerViewAdapter(
    private val user: MutableList<User>,
    private val removeOnClick: RemoveOnClick
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        fun onBind() {
            val model = user[adapterPosition]
            itemView.nameEditText.text = model.name.toEditable()
            itemView.surnameEditText.text = model.surname.toEditable()
            itemView.emailEditText.text = model.email.toEditable()
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            removeOnClick.onClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recycler_view_layout, parent, false)
        )
    }

    override fun getItemCount() = user.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    private fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)


}